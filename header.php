<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Misfit
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
	<!-- Facebook Pixel Code -->
	<script>
		!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
			n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
			n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
			t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
			document,'script','https://connect.facebook.net/en_US/fbevents.js');

		fbq('init', '1047073802031488');
		fbq('track', "PageView");</script>
	<noscript><img height="1" width="1" style="display:none"
				   src="https://www.facebook.com/tr?id=1047073802031488&ev=PageView&noscript=1"
		/></noscript>
	<!-- End Facebook Pixel Code -->
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<header id="masthead" class="site-header" role="banner">
		<div class="site-branding">
			<img src="<?php echo get_template_directory_uri() ?>/img/logoweb.svg">
			<span class="lema"><strike>Escuela</strike> para creativos</span>
		</div><!-- .site-branding -->
		<nav id="site-navigation" class="main-navigation" role="navigation">
			<a class="menu-button"></a>
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">
		<div class="iconos">
			<img class="icono circulo1" src="<?php echo get_template_directory_uri() ?>/img/iconosseccion2/circulo.svg">
			<img class="icono circulo2" src="<?php echo get_template_directory_uri() ?>/img/iconosseccion2/circulo.svg">
			<img class="icono cuadrado" src="<?php echo get_template_directory_uri() ?>/img/iconosseccion2/cuadrado.svg">
			<img class="icono triangulo1" src="<?php echo get_template_directory_uri() ?>/img/iconosseccion2/triangulo.svg">
			<img class="icono triangulo2" src="<?php echo get_template_directory_uri() ?>/img/iconosseccion2//triangulo.svg">
			<img class="icono pentagono" src="<?php echo get_template_directory_uri() ?>/img/iconosseccion2/hexagono.svg">
		</div>
