jQuery(document).ready(function(){

    jQuery('.overlayloader').addClass('activo');
    var ancho = jQuery(window).width();
    if(ancho>800 && jQuery('body').hasClass('home')) {
        var alto = jQuery(window).height();
        var nmsection = jQuery('.content li');
        var sec2 = jQuery('.texto').position();
        var operacion=ancho*4;
        jQuery('.secciones').width(ancho);
        jQuery('.home').width(ancho * nmsection.length);
        jQuery(window).scroll(function () {
            var scrollleft = jQuery(".home").scrollLeft();
            jQuery('.rectanguloschool').css({'-webkit-transform': 'translatey(' + scrollleft / 60 + '%)','transform': 'translatey(' + scrollleft / 60 + '%)','-moz-transform': 'translatey(' + scrollleft / 60 + '%)'});
            if (scrollleft >= ancho && scrollleft < ancho * 6) {
                if(!jQuery('.imgreferente').find('img').hasClass('visible')){
                    jQuery('.imgreferente').find('img').addClass('visible')
                }
                jQuery('.pregunta').css({'-webkit-transform':'translatex(' + scrollleft / 40 + '%)','transform':'translatex(' + scrollleft / 40 + '%)','-moz-transform':'translatex(' + scrollleft / 40 + '%)'});
                jQuery('.cuadrado2sec2').css({'-webkit-transform': 'translatex(-' + scrollleft / 30 + '%)','transform': 'translatex(-' + scrollleft / 30 + '%)','-moz-transform': 'translatex(-' + scrollleft / 30 + '%)'})
                jQuery('.respuesta').css({'-webkit-transform':'translatex(-' + scrollleft / 40 + '%)','transform':'translatex(-' + scrollleft / 40 + '%)','-moz-transform':'translatex(-' + scrollleft / 40 + '%)'});
                jQuery('.cuadrado2sec3').css({'-webkit-transform': 'translatex(' + scrollleft / 70 + '%)','transform': 'translatex(' + scrollleft / 70 + '%)','-moz-transform': 'translatex(' + scrollleft / 70 + '%)'})
                jQuery('.cuadradosec3').css({'-webkit-transform':'translate(-' + scrollleft / 60 + '%,-50%)','transform':'translate(-' + scrollleft / 60 + '%,-50%)','-moz-transform':'translate(-' + scrollleft / 60 + '%,-50%)'});
                jQuery('.formas2').css({'-webkit-transform':'translatex(-' + scrollleft / 60 + '%) skew(20deg)','transform':'translatex(-' + scrollleft / 60 + '%) skew(20deg)','-moz-transform':'translatex(-' + scrollleft / 60 + '%) skew(20deg)'});
                jQuery('.texto3 .titulo').css({'-webkit-transform':'translatex(-' + scrollleft/140  + '%)','transform':'translatex(-' + scrollleft/140 + '%)','-moz-transform':'translatex(-' + scrollleft/140  + '%)'});
                jQuery('.look').css({'-webkit-transform':'translatex(-' + scrollleft / 80 + '%)','transform':'translatex(-' + scrollleft / 80 + '%)','-moz-transform':'translatex(-' + scrollleft / 80 + '%)'});
                jQuery('.forma1').css({'-webkit-transform':'translatex(-' + scrollleft / 65 + '%) skew(20deg)','transform':'translatex(-' + scrollleft / 65 + '%) skew(20deg)','-moz-transform':'translatex(-' + scrollleft / 65 + '%) skew(20deg)'});
                jQuery('.forma2').css({'-webkit-transform':'translatex(-' + scrollleft / 55 + '%) skew(20deg)','transform':'translatex(-' + scrollleft / 55 + '%) skew(20deg)','-moz-transform':'translatex(-' + scrollleft / 55 + '%) skew(20deg)'});
                jQuery('.cuadrado2sec4').css({'-webkit-transform': 'translatex(' + scrollleft / 60 + '%)','transform': 'translatex(' + scrollleft / 60 + '%)','-moz-transform': 'translatex(' + scrollleft / 60 + '%)'});
            }

            if(scrollleft < ancho || scrollleft > ancho *2){
                jQuery('.imgreferente').find('img').removeClass('visible')
            }

            if(scrollleft > parseInt(operacion)+150){
                jQuery('.web3').addClass('visible')
            }

            if(scrollleft > ancho*2){
                jQuery('.walk').addClass('visible')
            }

            if(scrollleft > ancho*3){
                jQuery('.walk').removeClass('visible')
            }

            if(scrollleft < ancho*2){
                jQuery('.walk').removeClass('visible')
            }


            if(scrollleft < parseInt(operacion)+100){
                if(jQuery('.web3').hasClass('visible')) {
                    jQuery('.web3').removeClass('visible')
                }
            }

        })
        var alto = jQuery(window).height();
        var numesferas= alto/15;
        for (var i=1; i < Math.ceil(numesferas); i++){
            jQuery('.content_esfera').append('<div class="esfera"></div>');
        }
    }

    if(jQuery('body').hasClass('single')){
        jQuery('.iconos').hide();
    }

    if(ancho>800) {
        jQuery('a[href*=#]:not([href=#])').click(function () {
            jQuery('.menu-button').removeClass('active');
            jQuery('.menu').removeClass('visible');
            jQuery('.overlaytotal').removeClass('activo');
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = jQuery(this.hash);
                target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    position = target.offset().left;
                    jQuery('html,body').animate({
                        scrollLeft: position
                    }, 2000);
                    return false;
                }
            }
        });
    }else{
        jQuery('a[href*=#]:not([href=#])').click(function () {
            jQuery('.menu-button').removeClass('active');
            jQuery('.menu').removeClass('visible');
            jQuery('.overlaytotal').removeClass('activo');
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = jQuery(this.hash);
                target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    position = target.offset().top;
                    jQuery('html,body').animate({
                        scrollTop: position
                    }, 1000);
                    return false;
                }
            }
        });
    }

    jQuery('.more').click(function(){
        jQuery('.overlay').addClass('mostrar');
    })

    jQuery('.closeinstalaciones').click(function(){
        jQuery('.overlay').removeClass('mostrar');
    })

    jQuery('input').focus(function(){
        var valor = jQuery(this).val();
        jQuery('label').css('top','0px');
        if(valor!=''){
            jQuery('label').css('top','0px');
        }else{
            jQuery('label').css('bottom','0px');
        }

    })

    var imginstalaciones=jQuery('.gal-img');
    jQuery(imginstalaciones[0]).addClass('mostrar');

    jQuery('.next').click(function(){
        var posicion=jQuery('.galeria').find('.mostrar').data('posicion');
        var imgs = jQuery('.gal-img');
        if( posicion == jQuery(imgs).length-1){
            posicion = 0;
        }else{
            posicion++;
        }
        jQuery('.gal-img').removeClass('mostrar')
        jQuery(imgs[posicion]).addClass('mostrar');
    });

    jQuery('.prev').click(function(){
        var posicion=jQuery('.galeria').find('.mostrar').data('posicion');
        var imgs = jQuery('.gal-img');
        if( posicion == 0){
            posicion = jQuery(imgs).length-1;;
        }else{
            posicion--;
        }
        jQuery('.gal-img').removeClass('mostrar')
        jQuery(imgs[posicion]).addClass('mostrar');
    });

    jQuery('.menu-button').click(function(){
        jQuery(this).toggleClass('active');
        jQuery('.menu').toggleClass('visible');
        jQuery('.overlaytotal').toggleClass('activo');
    })

    jQuery('.carruselprof').owlCarousel({
        loop:false,
        nav:false,
        dots: true,
        URLhashListener:false,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    })

    jQuery('#contenido_temario').owlCarousel({
        loop:false,
        nav:false,
        dots: true,
        URLhashListener:false,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    })

    jQuery('.caruseltweet').owlCarousel({
        loop:false,
        nav:false,
        dots: true,
        URLhashListener:false,
        responsive:{
            0:{
                items:1,
                margin: 10,
            },
            600:{
                items:2,
                margin: 10,
            },
            1000:{
                items:2
            }
        }
    })

})

jQuery(window).load(function(){
    jQuery('.overlayloader').removeClass('activo');
})