(function($){
    $(document).ready(function() {
        var formfield;

        /* user clicks button on custom field, runs below code that opens new window */
        var custom_uploader;
        var images = [];
        var posicion = -1;
        var seccion='';
        $('.onetarek-upload-button').click(function(e) {
            e.preventDefault();

            seccion = jQuery(this).data('nameseccion');

            // obtiene el último div contenedor de las imagenes y respalda su posición

            if( $('.item_'+seccion+' div:last-of-type').data('position') != undefined ){
                posicion = $('.item_'+seccion+' div:last-of-type').data('position');
            }

            //If the uploader object has already been created, reopen the dialog
            if (custom_uploader) {
                custom_uploader.open();
                return;
            }
            //Extend the wp.media object
            custom_uploader = wp.media({
                title: 'Agregar imagen',
                button: {
                    text: 'Seleciona una imagen'
                },
                multiple: true
            });
            //When a file is selected, grab the URL and set it as the text field's value
            custom_uploader.on('select', function() {
                attachment = custom_uploader.state().get('selection').toJSON();
                if( $('#image_location_'+seccion).val() != ""){
                    //images = JSON.parse( $('#image_location').val() );
                    $('#image_location_'+seccion).val('');
                    $('.item_'+seccion).empty();
                    images = JSON.parse( $('#image_location_'+seccion).val() );
                }

                $.each( attachment, function( i, val ) {
                    console.log(val);
                    if(val.type=='image') {
                        images.push({
                            name: val.name,
                            thumbnail: val.sizes.thumbnail.url,
                            medium: val.sizes.medium.url,
                            full: val.sizes.full.url
                        });
                    }
                    posicion = parseInt(posicion) + parseInt(1);
                    $('.item_'+seccion).append('<div class="gal-img"><img src="'+val.sizes.thumbnail.url+'" ><a class="remove_img">Borrar</a></div>');
                });
                $('#image_location_'+seccion).val(JSON.stringify(images));
            });
            //Open the uploader dialog
            custom_uploader.open();
        });

        //$(".remove_img").click(function(){
        $('.item_sec2').on('click','.remove_img',function(){
            if( confirm("¿Esta seguro que desea eliminar esta imagen?") )
            {
                var arr = JSON.parse( $('#image_location_sec2').val() );
                arr.splice(0,1);
                $(this).parent().remove();
                $('#image_location_sec2').val('');
            }
        });

        $('.item_sec3').on('click','.remove_img',function(){
            if( confirm("¿Esta seguro que desea eliminar esta imagen?") )
            {
                var arr = JSON.parse( $('#image_location_sec3').val() );
                arr.splice(0,1);
                $(this).parent().remove();
                $('#image_location_sec3').val('');
            }
        });

        $('.item_sec5').on('click','.remove_img',function(){
            if( confirm("¿Esta seguro que desea eliminar esta imagen?") )
            {
                var arr = JSON.parse( $('#image_location_sec5').val() );
                arr.splice(0,1);
                $(this).parent().remove();
                $('#image_location_sec5').val('');
            }
        });

        $('.item_instalacionesimg').on('click','.remove_img',function(){
            if( confirm("¿Esta seguro que desea eliminar esta imagen?") )
            {
                var arr = JSON.parse( $('#image_location_instalacionesimg').val() );
                arr.splice(0,1);
                $(this).parent().remove();
                $('#image_location_instalacionesimg').val('');
            }
        });



        /*
         Please keep these line to use this code snipet in your project
         Developed by oneTarek http://onetarek.com
         */
        //adding my custom function with Thick box close function tb_close() .
        window.old_tb_remove = window.tb_remove;
        window.tb_remove = function() {
            window.old_tb_remove(); // calls the tb_remove() of the Thickbox plugin
            formfield=null;
        };

        // user inserts file into post. only run custom if user started process using the above process
        // window.send_to_editor(html) is how wp would normally handle the received data

        window.original_send_to_editor = window.send_to_editor;
        window.send_to_editor = function(html){
            if (formfield) {
                fileurl = $('img',html).attr('src');
                $(formfield).val(fileurl);
                tb_remove();
            } else {
                window.original_send_to_editor(html);
            }
        };

    });

})(jQuery);