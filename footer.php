<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Misfit
 */
?>

	<div class="overlaytotal"></div>
	<div class="overlayloader">
		<div class="centrado">
			<div class='loader'></div>
		</div>
	</div>
	</div><!-- #content -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
