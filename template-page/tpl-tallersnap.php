<?php
/*
* Template Name: Taller de snapchat
*/

get_header();

$temario = get_post_meta($post->ID,'temario',true);
$instructor = get_post_meta($post->ID,'instructor',true);

$args = array(
    'post_type' => 'instructores',
    'posts_per_page' => -1,
    'p' => $instructor,
    'order' => 'DESC'
);
$query = new WP_Query($args);

remove_filter( 'the_content', 'sharing_display',19 );
remove_filter( 'the_excerpt', 'sharing_display',19 );
?>
<style>
    body{
        overflow-y: auto;
    }
</style>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v2.6&appId=183877641957526";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<div class="tallersnap">
    <section class="portada">
        <div class="titulo">
            <img src="<?php echo get_template_directory_uri() ?>/img/titulotallersnap.png">
        </div>
    </section>
    <section class="descripcion">
        <div class="row">
            <?php
            while ( have_posts() ) : the_post();
                the_content();
            endwhile; // End of the loop.
            ?>
        </div>
    </section>
    <section class="likeseccion">
        <div class="row">
            <div class=" small-12 medium-12 large-6 columns">
                <p>Conecta con nosotros en Facebook</p>
            </div>
            <div class=" small-12 medium-12 large-6 columns">
                <div class="like">
                    <div class="fb-like" data-href="https://www.facebook.com/Misfit.org/?fref=ts" data-layout="standard" data-action="like" data-show-faces="true" data-share="false"></div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </section>
    <section class="keep">
        <div class="small-12 medium-6 large-6 columns img"></div>
        <div class="small-12 medium-6 large-6 columns texto">
            <div class="center">
            <h1>Keep snapping</h1>
            <p>Si las mejores aplicaciones de mensajería, foto y video tuvieran un extraño hijo híbrido, sería Snapchat, aprende a sacarle provecho y ¡keep snapping!</p>
            </div>
        </div>
    </section>
    <section class="temario">
        <div class="fondoblanco">
        <div class="row">
            <h2>Que aprenderás en el curso de snapchat</h2>
            <div class="content">
                <?php echo get_post_meta($post->ID,'temario',true); ?>
            </div>
        </div>
        </div>
    </section>
    <section class="instructor">
        <?php
        if($query->have_posts()) {
            while ($query->have_posts()) : $query->the_post();
                $feat_image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );?>

                <div class="small-12 medium-6 large-6 columns info_instructor">
                    <div class="center">
                        <p class="impartido">Impartido por</p>
                        <h1><?php echo get_the_title(); ?></h1>
                        <p><?php echo get_the_content(); ?></p>
                    </div>
                </div>
                <div style="background-image: url('<?php echo $feat_image; ?>')" class="small-12 medium-6 large-6 columns img_instructor"></div>

            <?php endwhile;
        }
        ?>
    </section>
    <section class="btn_pago">
        <div class="row">
            <div class="info_pago">
                <h1>Inversión</h1>
                <?php setlocale(LC_MONETARY, 'en_US'); ?>
                <p><?php echo money_format('%(#10n',$pago); ?></p>
            </div>
            <div class="botones">
                <div class="small-12 medium-6 large-6 columns efectivo">
                    <form action="https://compropago.com/comprobante" method="post" target="_blank">
                        <input type="hidden" name="public_key" value="pk_live_594369832280400ae">
                        <input type="hidden" class="total_res" name="product_price" value="<?php //echo $pago ?>">
                        <input id="nombre_item" type="hidden" name="product_name" value="Curso de snapchat">
                        <input class="id_pedido_res" type="hidden" name="product_id" value="">
                        <input type="hidden" name="customer_name" value="">
                        <input type="hidden" name="customer_email" value="">
                        <input type="hidden" name="customer_phone" value="">
                        <input type="hidden" name="image_url" value="">
                        <input type="hidden" name="success_url" value="">
                        <input type="hidden" name="failed_url" value="">
                        <div class="contenedor_btn"><input class="boton_pagos" type="submit" border="0" value="Efectivo" name="submit" alt="Pagar con ComproPago"></div>
                    </form>
                </div>

                <div class="small-12 medium-6 large-6 columns tarjeta">
                    <!--<form name="_xclick" action="https://www.paypal.com/mx/cgi-bin/webscr" method="post">
                        <input type="hidden" name="cmd" value="_xclick">
                        <input type="hidden" name="business" value="contacto@misfit.org.mx">
                        <input type="hidden" class="currency_code" name="currency_code" value="MXN">
                        <input id="nombre_item" type="hidden" name="item_name" value="">
                        <input type="hidden" class="total_res" name="amount" value="<?php //echo $pago ?>">
                        <input class="id_pedido_res" type="hidden" name="item_number" value="">
                        <div class="contenedor_btn"><input class="boton_pagos" type="submit" value="Tarjeta" border="0" name="submit" alt="PayPal, la forma más segura y rápida de pagar en línea."></div>
                    </form>-->
                    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank">
                        <input type="hidden" name="cmd" value="_s-xclick">
                        <input type="hidden" name="hosted_button_id" value="P283EFFHXGGD8">
                        <div class="contenedor_btn"><input class="boton_pagos" type="submit" border="0" name="submit" alt="PayPal, la forma más segura y rápida de pagar en línea." value="Tarjeta"></div>
                        <!--<img alt="" border="0" src="https://www.paypalobjects.com/es_XC/i/scr/pixel.gif" width="1" height="1">-->
                    </form>

                </div>
            </div>
        </div>
    </section>
    <section class="codigos">
        <div class="telefono"><p><img class="logowhats" src="<?php echo get_template_directory_uri() ?>/img/whatsApplogo.png"> 9992.71.32.62</p></div>
        <div class="row">
            <div class="small-12 medium-6 large-6 columns text-center">
                <img class="code" src="<?php echo get_template_directory_uri() ?>/img/fbcode.png">
            </div>
            <div class="small-12 medium-6 large-6 columns text-center">
                <img class="code" src="<?php echo get_template_directory_uri() ?>/img/snapcode.png">
            </div>
        </div>
    </section>
    <section class="contacto">
        <div class=" small-12 medium-6 large-6 columns imgcontacto"></div>
        <div class="small-12 medium-6 large-6 columns formcontacto">
            <div class="center"><?php echo do_shortcode('[contact-form-7 id="45" title="tallersnapthat"]'); ?></div>
        </div>
    </section>
</div>
<?php get_footer(); ?>
