<?php
/*
* Template Name: Home
*/
get_header();
$introduccion = get_post_meta($post->ID,'introduccion',true);
$sec2 = get_post_meta($post->ID,'sec2',true);
$sec3 = get_post_meta($post->ID,'sec3',true);
$sec4 = get_post_meta($post->ID,'sec4',true);
$sec5 = get_post_meta($post->ID,'sec5',true);
$imagenesinstalaciones = get_post_meta($post->ID,'instalacionesimg',true);
?>
<div id="1" class="home">
    <ul class="content">
    <li class="secciones portada">
        <img class="logo" src="<?php echo get_template_directory_uri() ?>/img/logonube.png">
        <img class="nube1" src="<?php echo get_template_directory_uri() ?>/img/nube1.png">
        <img class="nube2" src="<?php echo get_template_directory_uri() ?>/img/nube2.png">
        <img class="nube3" src="<?php echo get_template_directory_uri() ?>/img/nube1.png">
        <img class="nube4" src="<?php echo get_template_directory_uri() ?>/img/nube2.png">
        <img class="nube3" src="<?php echo get_template_directory_uri() ?>/img/nube2.png">
        <a href="#2"><img class="nextseccion2" src="<?php echo get_template_directory_uri() ?>/img/down.png"></a>
        <div class="content_esfera">
        </div>
    </li>
    <li id="2" class="secciones introduccion">
        <div class="large-6 columns explicacion">
            <div class="center">
                <h1 class="titulo"><strike style="color: #33D9C2"><span style="color: #2C2D2C;">Escuela</span></strike> para creativos</h1>
                <p><?php echo $introduccion ?></p>
            </div>
        </div>
        <div class="large-6 columns imgreferente">
            <div class="rectanguloschool"></div>
            <div class="center">
                <img src="<?php echo get_template_directory_uri() ?>/img/school3.jpg">
            </div>
        </div>

    </li>
    <li class="secciones texto">
        <div class="cuadradosec2"></div>
        <div class="cuadrado2sec2"></div>
        <div class="pregunta"><img class="interrogacion simbolo1" src="<?php echo get_template_directory_uri() ?>/img/interrogacionopen.svg"><h1><?php echo $sec2['pregunta'] ?></h1><img class="interrogacion simbolo2" src="<?php echo get_template_directory_uri() ?>/img/interrogacionclose.svg"></div>
        <div class="respuesta"><p><?php echo $sec2['texto'] ?></p></div>
        <a href="#3"><img class="nextseccion" src="<?php echo get_template_directory_uri() ?>/img/down2.png"></a>
    </li>
    <li id="3" class="secciones texto2">
        <div class="caja1">
            <div class="walk" style="background-image: url('<?php echo get_template_directory_uri() ?>/img/manwalking.jpg');"></div>
        </div>
        <div class="caja2">
            <div class="cuadrado2sec3"></div>
            <div class="cuadradosec3">
                <div class="contenido">
                    <p>Si alguna vez te has sentido así, como un </p>
                    <h1>Inadaptado</h1>
                    <div class="man" style="background-image: url('<?php echo get_template_directory_uri() ?>/img/manwalking.jpg')"></div>
                    <p>Entonces es momento de desafiar las barreras, desafiarte a ti mismo y hacer lo que amas cada vez que lo necesites.</p>
                </div>
            </div>
        </div>
        <a href="#4"><img class="nextseccion" src="<?php echo get_template_directory_uri() ?>/img/down2.png"></a>
    </li>
    <li id="4" class="secciones texto3">
        <div class="dude">
            <div class="forma1"></div>
            <div class="forma2"></div>
            <img class="imgdude" src="<?php echo get_template_directory_uri() ?>/img/dudemin.png">
            <img class="look" src="<?php echo get_template_directory_uri() ?>/img/dudelook.png">
        </div>
        <div class="informacion">
            <div class="titulo"><img class="comilla1" src="<?php echo get_template_directory_uri() ?>/img/comillas.svg" ><h1><?php echo $sec4['titulo'] ?></h1><img class="comilla2" src="<?php echo get_template_directory_uri() ?>/img/comillas.svg" >
                <div class="autor"><p>- Philippe Starck</p></div>
            </div>
            <div class="dudemovil" style="background-image: url('<?php echo get_template_directory_uri() ?>/img/dudecompleto.png')"></div>
            <div class="contenido">
                <div class="lineatop"></div>
                <p><?php echo $sec4['texto'] ?></p>
                <div class="lineabottom"></div>
            </div>
        </div>
        <a href="#5"><img class="nextseccion" src="<?php echo get_template_directory_uri() ?>/img/down2.png"></a>

    </li>
    <li id="5" class="secciones texto4">
        <div class="large-6 columns caja1">
            <div class="portada">
                <h1 class="vuelveempezar">Desármate y vuelve a empezar</h1>
                <div class="text-center contentimg"><img class="web3" src="<?php echo get_template_directory_uri() ?>/img/web3.jpg"></div>
            </div>
        </div>
        <div class="large-6 columns caja2">
        <div class="cuadrado2sec4"></div>
        <div class="center">
            <div class="contenedor">
                <div class="formcontacto"><?php echo do_shortcode('[contact-form-7 id="9" title="Formulario de contacto 1"]'); ?></div>
            </div>
            <div class="redes">
                <p class="text-center">Misfit <strike>Escuela</strike> para Creativos <?php echo date('Y') ?> &#0169;</p>
                <div class="glyph-icon flaticon-social"></div>
                <!--<div class="glyph-icon flaticon-social-media"></div>-->
                <div class="glyph-icon flaticon-camera"></div>
            </div>
        </div>
        </div>
        <!--<a href="#6"><img class="nextseccion" src="<?php echo get_template_directory_uri() ?>/img/down2.png"></a>-->
    </li>
    </ul>
</div>
<?php get_footer(); ?>