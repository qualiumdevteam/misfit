<?php
/*
* Template Name: Blog
*/

get_header();
$feat_image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

$args = array(
    'post_type' => 'post',
    'posts_per_page' => 12,
    'paged' => $paged,
    'order' => 'DESC'
);
$query = new WP_Query($args);
?>
<style>
    body{
        overflow-y: auto;
    }
</style>

<div class="blog">
    <section style="background-image: url('<?php echo get_template_directory_uri() ?>/img/blog.jpg')" class="portada">
        <div class="logo">
            <img src="<?php echo get_template_directory_uri() ?>/img/unet.png">

            <!--START Scripts : this is the script part you can add to the header of your theme-->
            <script type="text/javascript" src="http://misfit.org.mx/wp-includes/js/jquery/jquery.js?ver=2.7.1"></script>
            <script type="text/javascript" src="http://misfit.org.mx/wp-content/plugins/wysija-newsletters/js/validate/languages/jquery.validationEngine-es.js?ver=2.7.1"></script>
            <script type="text/javascript" src="http://misfit.org.mx/wp-content/plugins/wysija-newsletters/js/validate/jquery.validationEngine.js?ver=2.7.1"></script>
            <script type="text/javascript" src="http://misfit.org.mx/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.7.1"></script>
            <script type="text/javascript">
                /* <![CDATA[ */
                var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"http://misfit.org.mx/wp-admin/admin-ajax.php","loadingTrans":"Cargando..."};
                /* ]]> */
            </script><script type="text/javascript" src="http://misfit.org.mx/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.7.1"></script>
            <!--END Scripts-->

            <div class="widget_wysija_cont html_wysija">
                <div id="msg-form-wysija-html5716a83f15061-1" class="wysija-msg ajax"></div>
                <form id="form-wysija-html5716a83f15061-1" method="post" action="#wysija" class="widget_wysija html_wysija">
                    <p class="wysija-paragraph">
                        <input type="text" name="wysija[user][email]" class="wysija-input validate[required,custom[email]]" title="Email" placeholder="Email" value="" />
                        <span class="abs-req">
                            <input type="text" name="wysija[user][abs][email]" class="wysija-input validated[abs][email]" value="" />
                        </span>
                    </p>
                    <input class="wysija-submit wysija-submit-field" type="submit" value="Suscribir" />

                    <input type="hidden" name="form_id" value="1" />
                    <input type="hidden" name="action" value="save" />
                    <input type="hidden" name="controller" value="subscribers" />
                    <input type="hidden" value="1" name="wysija-page" />


                    <input type="hidden" name="wysija[user_list][list_ids]" value="1" />

                </form>
            </div>
        </div>
    </section>
    <section class="descripcion">
        <div class="row">
            <?php
            while ( have_posts() ) : the_post();
                the_content();
            endwhile; // End of the loop.
            ?>
            <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
                    <input type="search" class="search-field"
                           placeholder="<?php echo esc_attr_x( 'Ej. diseño industrial', 'placeholder' ) ?>"
                           value="<?php echo get_search_query() ?>" name="s"
                           title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
                <input type="submit" class="search-submit"
                       value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
            </form>
        </div>
    </section>
    <section class="listado">
        <?php
        if($query->have_posts()) {
            $contador=1;
            while ($query->have_posts()) : $query->the_post();
                $feat_image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );
                if($contador==1){
                    echo '<div class="contenedor">';
                }
                ?>
                <a href="<?php echo get_the_permalink(get_the_ID()); ?>">
                    <div class="small-12 medium-6 large-6 columns item">
                        <div style="background-image: url('<?php echo $feat_image; ?>')" class="imgdestacada"></div>
                        <h3 class="titulo"><?php echo get_the_title(); ?></h3>
                        <p class="extracto"><?php echo strip_tags(substr(get_the_content(),0,130)); ?>...</p>
                </div>
                </a>
                <?php
                if($contador==2){
                    echo '</div>';
                    $contador=0;
                }
                $contador++;
                ?>
            <?php endwhile;
        }
        ?>
        <div class="clearfix"></div>
        <div class="paginacion text-center"><span class="info_page">P.</span> <div class="number_page"><?php echo get_pagination($query) ?></div></div>
    </section>
    <section class="footer">
        <div class="small-12 medium-6 large-6 columns text-left">
            <p><a target="_blank" href="<?php echo get_the_permalink(84); ?>">Aviso de privacidad</a></p>
        </div>
        <div class="small-12 medium-6 large-6 columns text-right">
            <p>&copy; 2016 Misfit</p>
        </div>
    </section>
</div>
<?php get_footer(); ?>
