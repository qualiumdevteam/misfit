<?php
/*
* Template Name: Rebel camp
*/
get_header();
$textobanner = get_post_meta($post->ID,'textobanner',true);
$instructores = get_post_meta($post->ID,'instructores',true);
?>
<style>
    body{
        overflow-y: auto;
    }
</style>
<?php
remove_filter( 'the_content', 'sharing_display',19 );
remove_filter( 'the_excerpt', 'sharing_display',19 );
?>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
<div class="rebel_camp">
    <section class="portada">
        <div class="center">
            <img src="<?php echo get_template_directory_uri() ?>/img/titulo_rebel.png">
        </div>
    </section>
    <section class="descripcion">
        <div class="row">
            <h1 class="titulo">¿Estás listo?</h1>
            <?php
            while ( have_posts() ) : the_post();
                nl2br(the_content());
            endwhile; // End of the loop.
            ?>
            <?php
            if ( function_exists( 'sharing_display' ) ) {
                sharing_display( '', true );
            }
            if ( class_exists( 'Jetpack_Likes' ) ) {
                $custom_likes = new Jetpack_Likes;
                echo $custom_likes->post_likes( '' );
            }
            ?>
        </div>
    </section>
    <section class="bannerrebel1">
        <div class="row">
            <?php echo nl2br($textobanner); ?>
        </div>
    </section>
    <section class="profesores">
        <h1 class="titulo">Profesores</h1>
        <div class="carruselprof">
            <?php
                foreach($instructores as $item_prof){ ?>
                    <div class="item">
                        <div class="small-12 medium-6 large-6 columns texto">
                            <h1 class="nombre"><?php echo get_the_title($item_prof); ?></h1>
                            <p><?php echo nl2br(get_post_field('post_content', $item_prof)); ?></p>
                            <a class="linkliked" target="_blank" href="<?php echo get_post_meta($item_prof,'likedin',true); ?>">Ver en LinkedIn</a>
                        </div>
                        <div style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id($item_prof)); ?>')" class="small-12 medium-6 large-6 columns imagen">

                        </div>
                    </div>
                <?php }
            ?>
        </div>
    </section>
    <section class="frase">
        <div class="row">
            <h1>Creativity is intelligence</h1>
            <h1>having fun</h1>
            <p>- Albert Einstein</p>
        </div>
    </section>
    <section class="temario">
        <h1 class="titulo">Temario</h1>
        <div class="row">
            <div id="contenido_temario" class="contenido">
                <div class="item">
                    <h3>Conceptualización</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum dolor sit amet iure dolor in reprendin.</p>
                </div>
            </div>
        </div>
    </section>
    <section class="btn_costos">
        <div class="fondo">
            <div class="row">
                <div class="info_pago">
                    <h1>Inversión</h1>
                    <p> $ 6.800</p>
                </div>
                <div class="botones">
                    <div class="small-12 medium-6 large-6 columns efectivo">
                        <form action="https://compropago.com/comprobante" method="post" target="_blank">
                            <input type="hidden" name="public_key" value="pk_live_594369832280400ae">
                            <input type="hidden" class="total_res" name="product_price" value="6800">
                            <input id="nombre_item" type="hidden" name="product_name" value="Curso de snapchat">
                            <input class="id_pedido_res" type="hidden" name="product_id" value="">
                            <input type="hidden" name="customer_name" value="">
                            <input type="hidden" name="customer_email" value="">
                            <input type="hidden" name="customer_phone" value="">
                            <input type="hidden" name="image_url" value="">
                            <input type="hidden" name="success_url" value="">
                            <input type="hidden" name="failed_url" value="">
                            <div class="contenedor_btn"><input class="boton_pagos" type="submit" border="0" value="Efectivo" name="submit" alt="Pagar con ComproPago"></div>
                        </form>
                    </div>

                    <div class="small-12 medium-6 large-6 columns tarjeta">
                        <form name="_xclick" action="https://www.paypal.com/mx/cgi-bin/webscr" method="post">
                            <input type="hidden" name="cmd" value="_xclick">
                            <input type="hidden" name="business" value="contacto@misfit.org.mx">
                            <input type="hidden" class="currency_code" name="currency_code" value="MXN">
                            <input id="nombre_item" type="hidden" name="item_name" value="">
                            <input type="hidden" class="total_res" name="amount" value="6800">
                            <input class="id_pedido_res" type="hidden" name="item_number" value="">
                            <div class="contenedor_btn"><input class="boton_pagos" type="submit" value="Tarjeta" border="0" name="submit" alt="PayPal, la forma más segura y rápida de pagar en línea."></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="tweets">
        <h1>Sé parte de la comunidad</h1>
        <div class="caruseltweet">
            <div class="item">
                <blockquote class="twitter-tweet" data-lang="es"><p lang="en" dir="ltr">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut quam metus, imperdiet nec rhoncus euismod, euismod a lorem. Integer ante dolor.</p>&mdash; jose luis diaz (@josediazh25) <a href="https://twitter.com/josediazh25/status/725807863243804672">28 de abril de 2016</a></blockquote>
            </div>
            <div class="item">
                <blockquote class="twitter-tweet" data-lang="es"><p lang="en" dir="ltr">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut quam metus, imperdiet nec rhoncus euismod, euismod a lorem. Integer ante dolor.</p>&mdash; jose luis diaz (@josediazh25) <a href="https://twitter.com/josediazh25/status/725807863243804672">28 de abril de 2016</a></blockquote>
            </div>
        </div>
    </section>
    <section class="codigos">
        <div class="fondo">
            <div class="row">
                <div class="small-12 medium-6 large-6 columns text-center">
                    <img class="code" src="<?php echo get_template_directory_uri() ?>/img/fbcode.png">
                </div>
                <div class="small-12 medium-6 large-6 columns text-center">
                    <img class="code" src="<?php echo get_template_directory_uri() ?>/img/snapcode.png">
                </div>
            </div>
        </div>
    </section>
    <section class="contacto">
        <div class=" small-12 medium-6 large-6 columns imgcontacto"></div>
        <div class="small-12 medium-6 large-6 columns formcontacto">
            <div class="center">
                <?php echo do_shortcode('[contact-form-7 id="45" title="tallersnapthat"]'); ?>
            </div>
        </div>
    </section>
</div>
<?php get_footer(); ?>
