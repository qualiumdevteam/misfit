<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Misfit
 */

get_header();

$banner = get_post_meta(58,'banner',true);
$feat_image_banner = wp_get_attachment_url( get_post_thumbnail_id($banner) );

$args = array(
	'post_type' => 'post',
	'posts_per_page' => 5,
	'order' => 'DESC'
);
$query = new WP_Query($args);
?>
	<style>
		body{
			overflow-y: auto;
		}
	</style>

	<div id="primary" class="content_articulo content-area">
		<div class="small-12 medium-4 large-3 columns articulos_relacionados">
			<div class="divflotante"></div>
			<h3 class="titulo">Otros articulos</h3>
			<?php
			if($query->have_posts()) {
				while ($query->have_posts()) : $query->the_post();
					$feat_image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );?>
					<a href="<?php echo get_the_permalink(get_the_ID()); ?>">
						<div class="small-12 medium-12 large-12 columns item">
							<div style="background-image: url('<?php echo $feat_image; ?>')" class="imgdestacada"></div>
							<h2 class="titulo"><?php echo get_the_title(); ?></h2>
						</div>
					</a>
				<?php
				endwhile;
				wp_reset_query();
			}
			?>
		</div>
		<div class="small-12 medium-8 large-9 columns articulo">
		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content', get_post_format() );

			//the_post_navigation();

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>
		<div class="text-center banner"><img src="<?php echo $feat_image_banner; ?>"></div>
		</div>
	</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();
