<?php
add_action('add_meta_boxes', 'cyb_meta_boxes');
function cyb_meta_boxes()
{
    $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post'];
    $template_file = get_post_meta($post_id, '_wp_page_template', TRUE);
    if ($template_file == 'template-page/tpl-home.php') {
        add_meta_box('cyb-meta-introduccion', __('Seccion escuela'), 'cyb_meta_introduccion', 'page');
        add_meta_box('cyb-meta-seccion2', __('Seccion2'), 'cyb_meta_seccion2', 'page');
        add_meta_box('cyb-meta-seccion3', __('Seccion3'), 'cyb_meta_seccion3', 'page');
        add_meta_box('cyb-meta-seccion4', __('Seccion4'), 'cyb_meta_seccion4', 'page');
        add_meta_box('cyb-meta-instalaciones', __('Instalaciones'), 'cyb_meta_instalaciones', 'page');
        add_meta_box('cyb-meta-instalacionesimg', __('Imagenes de las instalaciones'), 'cyb_meta_instalacionesimg', 'page');
    }
    if ($template_file == 'template-page/tpl-tallersnap.php') {
        add_meta_box('cyb-meta-temario', __('Temario'), 'cyb_meta_temario', 'page');
        add_meta_box('cyb-meta-instructor', __('Instructor'), 'cyb_meta_instructor', 'page');
    }

    if ($template_file == 'template-page/tpl-rebel.php') {
        add_meta_box('cyb-meta-textobanner', __('Texto banner'), 'cyb_meta_textobanner', 'page');
        add_meta_box('cyb-meta-instructores', __('instructores'), 'cyb_meta_instructores', 'page');
    }

    if ($template_file == 'template-page/tpl-blog.php') {
        add_meta_box('cyb-meta-banner', __('Banner de los articulo'), 'cyb_meta_banner', 'page');
    }

    add_meta_box('cyb-meta-linkliked', __('Enlace del likedin'), 'cyb_meta_linkliked', 'instructores');
}

function cyb_meta_linkliked(){
    global $post;
    $instructores = get_post_meta($post->ID,'likedin',true);

    $html='<div>';
    $html.='<input type="text" name="likedin" value="'.$instructores.'">';
    $html .='</div>';

    echo $html;
}


function cyb_meta_instructores(){

    global $post;
    $instructores = get_post_meta($post->ID,'instructores',true);

    $option_instructor='';
    $args_instructores = array(
        'post_type' => 'instructores',
        'posts_per_page' => -1,
        'order' => 'DESC'
    );
    $query_instructores = new WP_Query($args_instructores);
    if($query_instructores->have_posts()) {
        while ($query_instructores->have_posts()) : $query_instructores->the_post();
            if(in_array(get_the_ID(),$instructores)) {
                $option_instructor .= '<input checked="checked" type="checkbox" name="instructores[]" value="' . get_the_ID() . '">' . get_the_title() . '<br><br>';
            }else{
                $option_instructor .= '<input type="checkbox" name="instructores[]" value="' . get_the_ID() . '">' . get_the_title() . '<br><br>';
            }
        endwhile;
    };

    wp_reset_query();

    $html ='<div>';
    $html .= $option_instructor;
    $html .='</div>';
    echo $html;
}

function cyb_meta_textobanner(){
    global $post;
    $textobanner = get_post_meta($post->ID,'textobanner',true);
    $settings = array('wptextobanner' => true, 'media_buttons' => true, 'quicktags' => true, 'textarea_rows' => '15', 'textarea_name'=>'textobanner' );

    $html ='<div>';
    $html .= wp_editor(wp_kses_post($textobanner , ENT_QUOTES, 'UTF-8'), 'content_textobanner', $settings);
    $html .='</div>';

    echo $html;
}

function cyb_meta_banner(){
    global $post;
    $banner = get_post_meta($_GET['post'],'banner',true);
    $option_banner='';

    $args_banner = array(
        'post_type' => 'banner',
        'posts_per_page' => -1,
        'order' => 'DESC'
    );
    $query_banner = new WP_Query($args_banner);
    if($query_banner->have_posts()) {
        while ($query_banner->have_posts()) : $query_banner->the_post();
            if($banner == get_the_ID()) {
                $option_banner .= '<option selected value="' . get_the_ID() . '">' . get_the_title() . '</option>';
            }else{
                $option_banner .= '<option value="' . get_the_ID() . '">' . get_the_title() . '</option>';
            }
        endwhile;
    }
    wp_reset_query();

    $html ='<div>';
    $html .='<select name="banner">';
    $html .='<option value="">Selecciona una opción</option>';
    $html .= $option_banner;
    $html .='</select>';
    $html .='</div>';
    echo $html;
}

function cyb_meta_instructor(){
    global $post;
    $instructor = get_post_meta($post->ID,'instructor',true);
    $option='';

    $args = array(
        'post_type' => 'instructores',
        'posts_per_page' => -1,
        'order' => 'DESC'
    );
    $query = new WP_Query($args);
    if($query->have_posts()) {
        while ($query->have_posts()) : $query->the_post();
            if($instructor == get_the_ID()) {
                $option .= '<option selected value="' . get_the_ID() . '">' . get_the_title() . '</option>';
            }else{
                $option .= '<option value="' . get_the_ID() . '">' . get_the_title() . '</option>';
            }
        endwhile;
    }
    wp_reset_query();

        $html ='<div>';
        $html .='<select name="instructor">';
        $html .='<option value="">Selecciona una opción</option>';
        $html .= $option;
        $html .='</select>';
        $html .='</div>';
    echo $html;
}

function cyb_meta_temario(){
    global $post;
    $temario = get_post_meta($post->ID,'temario',true);
    $settings = array('wptemario' => true, 'media_buttons' => true, 'quicktags' => true, 'textarea_rows' => '15', 'textarea_name'=>'temario' );

    $html ='<div>';
    $html .= wp_editor(wp_kses_post($temario , ENT_QUOTES, 'UTF-8'), 'content_temario', $settings);
    $html .='</div>';

    echo $html;
}

function cyb_meta_introduccion(){
    global $post;
    $introduccion = get_post_meta($post->ID,'introduccion',true);

    $html ='<div>';
    $html .= '<textarea style="width: 100%; height: 150px;" name="introduccion" placeholder="">'.$introduccion.'</textarea><br><br>';
    $html .='</div>';

    echo $html;
}

function cyb_meta_seccion2(){
    global $post;
    $sec2 = get_post_meta($post->ID,'sec2',true);

    $html='<div>';
    $html .= '<label>Titulo</label><br>';
    $html .= '<input type="text" name="pregunta" value="'.$sec2['pregunta'].'" placeholder="Titulo"><br><br>';
    $html .= '<label>Texto</label><br>';
    $html .= '<textarea style="width: 100%; height: 150px;" name="texto" placeholder="Texto">'.$sec2['texto'].'</textarea><br><br>';
    $html .='</div>';


    echo $html;
}

function cyb_meta_seccion3(){
    global $post;
    $sec3 = get_post_meta($post->ID,'sec3',true);
    $settings = array('wpautop' => true, 'media_buttons' => true, 'quicktags' => true, 'textarea_rows' => '15', 'textarea_name'=>'content_seccion_tres' );

    $html='<div>';
    $html .= '<label>Texto</label><br>';
    $html .= wp_editor(wp_kses_post($sec3 , ENT_QUOTES, 'UTF-8'), 'secciontres', $settings);
    $html .='</div>';

    echo $html;
}

function cyb_meta_seccion4(){
    global $post;
    $sec4 = get_post_meta($post->ID,'sec4',true);
    $settings = array('wpautop' => true, 'media_buttons' => true, 'quicktags' => true, 'textarea_rows' => '15', 'textarea_name'=>'content_seccion_cuatro' );

    $html='<div>';
    $html .= '<label>Titulo</label><br>';
    $html .= '<input type="text" name="titulo" value="'.$sec4['titulo'].'" placeholder="Titulo"><br><br>';
    $html .= '<label>Texto</label><br>';
    $html .= wp_editor(wp_kses_post($sec4['texto'] , ENT_QUOTES, 'UTF-8'), 'seccioncuatro', $settings);
    $html .='</div>';

    echo $html;
}

function cyb_meta_instalaciones(){
    global $post;
    $sec5 = get_post_meta($post->ID,'sec5',true);
    $settings = array('wpautop' => true, 'media_buttons' => false, 'quicktags' => true, 'textarea_rows' => '15', 'textarea_name'=>'content_seccion_instalacion' );

    $html='<div>';
    $html .= '<label>Portada</label><br>';
    $html .= "<input style='display:none;' id='image_location_sec5' type='text' name='imgportada' value='".$sec5['imgportada']."'>";
    $html .= '<input data-nameseccion="sec5" type="button" class="onetarek-upload-button button" id="wp_custom_attachment" name="wp_custom_attachment" value="Agregar imagen" /><br><br>';
    $html .= '<div class="item_sec5">';
    if($sec5['imgportada']!='') {
        $html .= '<div class="gal-img"><img src="' . json_decode($sec5['imgportada'])[0]->thumbnail . '"><a class="remove_img">Borrar</a></div>';
    }
    $html .= '</div>';
    $html .= wp_editor(wp_kses_post($sec5['texto'] , ENT_QUOTES, 'UTF-8'), 'seccioncinco', $settings);
    $html .='</div>';

    echo $html;
}

function cyb_meta_instalacionesimg(){
    global $post;
    $imagenesinstalaciones = get_post_meta($post->ID,'instalacionesimg',true);
    $html='<div>';
    $html .= '<label>Imagenes</label><br>';
    $html .= "<input style='display:none;' id='image_location_instalacionesimg' type='text' name='instalacionesimg' value='".$imagenesinstalaciones."'>";
    $html .= '<input data-nameseccion="instalacionesimg" type="button" class="onetarek-upload-button button" id="wp_custom_attachment" name="wp_custom_attachment" value="Agregar imagen" /><br><br>';
    $html .= '<div class="item_instalacionesimg">';
    if($imagenesinstalaciones!='') {
        foreach(json_decode($imagenesinstalaciones) as $img) {
            $html .= '<div class="gal-img"><img src="' .$img->thumbnail . '"><a class="remove_img">Borrar</a></div>';
        }
    }
    $html .= '</div>';
    $html .='</div>';

    echo $html;
}

add_action( 'save_post', 'dynamic_save_postdata' );
/**
 * @param $post_id
 */
function dynamic_save_postdata($post_id ) {

    if( isset( $_POST['introduccion'])){
        $introduccion=$_POST['introduccion'];
        update_post_meta($post_id,'introduccion',$introduccion);
    }

    if( isset( $_POST['texto']) || isset($_POST['pregunta'])){
        $datossec2=array('texto' => $_POST['texto'],'pregunta'=>$_POST['pregunta']);
        update_post_meta($post_id,'sec2',$datossec2);
    }

    if( isset( $_POST['content_seccion_tres'])){
        update_post_meta($post_id,'sec3',$_POST['content_seccion_tres']);
    }

    if( isset( $_POST['content_seccion_cuatro']) || isset($_POST['titulo'])){
        $datossec4=array('texto' => $_POST['content_seccion_cuatro'],'titulo'=>$_POST['titulo']);
        update_post_meta($post_id,'sec4',$datossec4);
    }

    if( isset( $_POST['content_seccion_instalacion']) || isset($_POST['imgportada'])){
        $datossec5=array('texto' => $_POST['content_seccion_instalacion'],'imgportada'=>$_POST['imgportada']);
        update_post_meta($post_id,'sec5',$datossec5);
    }

    if( isset( $_POST['instalacionesimg'])){
        $datosinstalacionesimg=$_POST['instalacionesimg'];
        update_post_meta($post_id,'instalacionesimg',$datosinstalacionesimg);
    }

    if( isset( $_POST['temario'])){
        $datostemario=$_POST['temario'];
        update_post_meta($post_id,'temario',$datostemario);
    }

    if( isset( $_POST['instructor'])){
        $datosinstructor=$_POST['instructor'];
        update_post_meta($post_id,'instructor',$datosinstructor);
    }

    if( isset( $_POST['banner'])){
        $datosbanner=$_POST['banner'];
        update_post_meta($post_id,'banner',$datosbanner);
    }

    if( isset( $_POST['textobanner'])){
        $datostextobanner=$_POST['textobanner'];
        update_post_meta($post_id,'textobanner',$datostextobanner);
    }

    if( isset( $_POST['instructores'])){
        $datosinstructores=$_POST['instructores'];
        update_post_meta($post_id,'instructores',$datosinstructores);
    }

    if( isset( $_POST['likedin'])){
        $datoslikedin=$_POST['likedin'];
        update_post_meta($post_id,'likedin',$datoslikedin);

    }
}