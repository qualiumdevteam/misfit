<?php
add_action( 'init', 'create_post_type' );
function create_post_type()
{
    register_post_type('Instructores',
        array(
            'labels' => array(
                'name' => __('instructores'),
                'singular_name' => __('instructor')
            ),
            'public' => true,
            'has_archive' => true,
            'taxonomies' => array(),
            'supports' => array('title','editor','thumbnail')
        )
    );

    register_post_type('Banner',
        array(
            'labels' => array(
                'name' => __('banner'),
                'singular_name' => __('banner')
            ),
            'public' => true,
            'has_archive' => true,
            'taxonomies' => array(),
            'supports' => array('title','thumbnail')
        )
    );
}